

# Neorg Cheatsheet
  - Getting Help
    -- You can always get help by entering /:h neorg/
# _Headers_
# Header 1 : 1 star
## Header 2 : 2 stars
### Header3 : 3 stars
#### Header 4 : 4 stars
##### Header 5 : 5 stars
###### Header 6 : 6 stars 

# Styling
# text-bold*: asterisk

# text-underline_: underscore

# text-italics/ : slash

-text-strikethrough- : dash

### File Navigation

    {:NAME_OF_FILE:}  -- Takes me to the file
    {* NAME_OF_HEADING} -- it looks to the heading, is specific to heading type.
    {# NAME_OF_SECTION} -- # is a wildcard.

#### :Neorg index 
    - puts you on the index no matter where you are.
#### :Neorg return 
    - returns you to where you were if you were using neovim.
    -- closes all neorg buffers


## Todo Items

   - ( ) Make an ordered list and toggle the items by pressing /Ctrl + Space/.

## posting images

@code python
def main():
  pass
   @end


# Test

# kaljsdl* ;kfja; /sldkfjas/ ;ldkjfalekdfa

## Test
   fjdaklsjf;_slkdjfs_ -akldjfasasdfsa-

### Test

    fdalksjdf;aklsd **jlkjdf;ldkjf** ;aslkdjfa;lskdjf;alskdfja;slkdjfas;lkdjf;dkljas

#### Test
     fjaklsjdfa;dkfja;sdlkjf;asslkdjf;asldkjf;aslkdjf;alskdjfa;slkdjf

##### Test
      fjdkals;jdflaskjdfa;sdklfj;asldkjfa;sldkjfa;slkdjfa

###### Test 
       jfdl;akjsdf;lkdjf;aslkdj;ldkjfa;lkdjal.

@image 












