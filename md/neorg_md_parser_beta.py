#!/usr/bin/python3
import os
import re

PROMPT = input('Would you like to parse a norg file into a markdown file? ').upper()

if PROMPT == 'Y':
    files = os.listdir('.')
    filenames_without_extensions = []
    for f in files:
        if f.endswith('.norg'):
            filename, extension = os.path.splitext(f)
            filenames_without_extensions.append(filename)

    print(str(filenames_without_extensions))

elif PROMPT == 'N':
    print('See you later alligator.')
    exit()
else:
    print('Please try again with a valid input.')
    exit()

def declare_file_to_parse():
    """declare which file base name to parse"""
    return input("Which file do you want to parse into HTML (Please don't include file extension)? ")

PF = declare_file_to_parse()

def open_file():

    with open(f"{PF}.norg", 'r') as input_file:
        return input_file.read()

def check_html_file():
    """Creating an html folder if it already exists."""
    if not os.path.exists('markdown'):
        os.makedirs('markdown')

def create_tmp_files():
    tmp_file_path = os.path.join("markdown", "tmp.txt")
    tmp1_file_path = os.path.join("markdown", "tmp1.txt")

    with open(tmp_file_path, "w") as tmp_file:
        tmp_file.write("This is the content of tmp.txt file.\nYou can add more content here if needed.")

    with open(tmp1_file_path, "w") as tmp1_file:
        tmp1_file.write("This is the content of tmp1.txt file.\nYou can add more content here if needed.")


def yank_from_html_and_save_to_tmp(html_file_path=f'markdown/{PF}.md'):
    # Define a regular expression pattern for lines to yank
    yank_pattern = re.compile(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link}).*?)$')

    # Open the HTML file and create or open tmp.txt
    tmp_file_path = os.path.join("markdown", "tmp1.txt")

    with open(html_file_path, 'r') as html_file, open(tmp_file_path, 'w') as tmp_file:
        for line in html_file:
            match = yank_pattern.match(line)
            if match:
                yanked_text = match.group(1)
                tmp_file.write(f'{yanked_text}\n')

    return None

def neorg_to_markdown_header(line):
    """
    Convert neorg header to markdown header.

    Args:
        line (str): The neorg header line.

    Returns:
        str: The corresponding markdown header.
    """
    # Check if the line is empty
    if not line:
        return line

    # Check if the line starts with specific prefixes
    prefixes = ['@code', '@image', '@end']  # Add more prefixes as needed
    if any(line.startswith(prefix) for prefix in prefixes):
        return line

    # Check if the line starts with a letter
    if line[0].isalpha():
        return line
    else:
        # Check if the line starts with hyphens or whitespace
        if line.startswith('-') or line.startswith(' '):
            return line
        else:
            # Count the number of '*' characters at the beginning of the line
            count = 0
            for char in line:
                if char == '*':
                    count += 1
                else:
                    break

            # Ensure the count is between 1 and 6
            count = min(max(count, 1), 6)

            # Convert '*' to '#' for markdown headers
            markdown_header = '#' * count + ' ' + line[count:].lstrip()

            return markdown_header

def format_bold_text(line):
    """
    Format bold text in the line by adding double asterisks.

    Args:
        line (str): The line of text.

    Returns:
        str: The line with formatted bold text.
    """
    # Define a regular expression pattern for bold text enclosed by asterisks
    bold_pattern = re.compile(r'\*(.*?)\*')

    # Replace matches with double asterisks
    formatted_line = bold_pattern.sub(r'**\1**', line)

    return formatted_line

def create_namespaces(html_file_path=f'markdown/{PF}.md'):
    tmp_file_path = os.path.join("markdown", "tmp.txt")
    tmp1_file_path = os.path.join("markdown", "tmp1.txt")

    tmp_content = [f'__YANKED_CONTENT__{i}__\n' for i, line in enumerate(open(html_file_path, 'r')) if re.match(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link|photo}).*?)$', line)]
    
    with open(tmp1_file_path, 'w') as tmp1_file:
        tmp1_file.writelines(f'{namespace} {line.strip()}\n' for namespace, line in zip(tmp_content, open(html_file_path, 'r')) if re.match(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link|photo}).*?)$', line))

    with open(tmp_file_path, 'w') as tmp_file:
        tmp_file.writelines(line for line in open(html_file_path, 'r') if '__YANKED_CONTENT__' not in line)


def paste_namespaces():
    # Call the create_namespaces function
    create_namespaces()

    # Read the content from tmp.txt, excluding the last four lines
    with open("markdown/tmp.txt", "r") as tmp_file:
        tmp_content = tmp_file.readlines()

    # Write the content back to the HTML file
    with open(f"markdown/{PF}.md", "w") as html_file:
        html_file.writelines(tmp_content)

# Apply the methods one by one
original_content = open_file()
check_html_file()
create_tmp_files()

# Convert neorg headers to markdown headers line by line
converted_content = [neorg_to_markdown_header(line) for line in original_content.split('\n')]

# Apply the format_bold_text function
converted_content = [format_bold_text(line) for line in converted_content]

# Join the lines and write to the output file
with open(f"markdown/{PF}.md", "w") as output_file:
    output_file.write('\n'.join(converted_content))

yank_from_html_and_save_to_tmp()
create_namespaces()
paste_namespaces()
