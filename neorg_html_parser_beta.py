#!/usr/bin/python3
import os
import re

PROMPT = input('Would you like to parse a norg file into html? ').upper()

if PROMPT == 'Y':
    files = os.listdir('.')
    filenames_without_extensions = []
    for f in files:
        if f.endswith('.norg'):
            filename, extension = os.path.splitext(f)
            filenames_without_extensions.append(filename)

    print(str(filenames_without_extensions))

elif PROMPT == 'N':
    print('See you later alligator.')
    exit()
else:
    print('Please try again with a valid input.')
    exit()

def declare_file_to_parse():
    """declare which file base name to parse"""
    return input("Which file do you want to parse into HTML (Please don't include file extension)? ")

PF = declare_file_to_parse()

def open_file():

    with open(f"{PF}.norg", 'r') as input_file:
        return input_file.read()

def check_html_file():
    """Creating an html folder if it already exists."""
    if not os.path.exists('html'):
        os.makedirs('html')

def create_tmp_files():
    tmp_file_path = os.path.join("html", "tmp.txt")
    tmp1_file_path = os.path.join("html", "tmp1.txt")

    with open(tmp_file_path, "w") as tmp_file:
        tmp_file.write("This is the content of tmp.txt file.\nYou can add more content here if needed.")

    with open(tmp1_file_path, "w") as tmp1_file:
        tmp1_file.write("This is the content of tmp1.txt file.\nYou can add more content here if needed.")

def yank_from_html_and_save_to_tmp(html_file_path=f'html/{PF}.html'):
    # Define a regular expression pattern for lines to yank
    yank_pattern = re.compile(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link}).*?)$')

    # Open the HTML file and create or open tmp.txt
    tmp_file_path = os.path.join("html", "tmp1.txt")

    with open(html_file_path, 'r') as html_file, open(tmp_file_path, 'w') as tmp_file:
        for line in html_file:
            match = yank_pattern.match(line)
            if match:
                yanked_text = match.group(1)
                tmp_file.write(f'{yanked_text}\n')

    return None

def create_namespaces(html_file_path=f'html/{PF}.html'):
tmp_file_path = os.path.join("html", "tmp.txt")
    tmp1_file_path = os.path.join("html", "tmp1.txt")

    tmp_content = [f'__YANKED_CONTENT__{i}__\n' for i, line in enumerate(open(html_file_path, 'r')) if re.match(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link|photo}).*?)$', line)]
    
    with open(tmp1_file_path, 'w') as tmp1_file:
        tmp1_file.writelines(f'{namespace} {line.strip()}\n' for namespace, line in zip(tmp_content, open(html_file_path, 'r')) if re.match(r'^\s*(.*(?:{:|{#|\{\*|http|ftp|link|photo}).*?)$', line))

    with open(tmp_file_path, 'w') as tmp_file:
        tmp_file.writelines(line for line in open(html_file_path, 'r') if '__YANKED_CONTENT__' not in line)

def paste_namespaces():
    # Call the create_namespaces function
    create_namespaces()

    # Read the content from tmp.txt, excluding the last four lines
    with open("html/tmp.txt", "r") as tmp_file:
        tmp_content = tmp_file.readlines()

    # Write the content back to the HTML file
    with open(f"html/{PF}.html", "w") as html_file:
        html_file.writelines(tmp_content)

def convert_code_blocks(content):
    lines = content.split('\n')
    in_code_block = False
    code_language = ''
    code_lines = []
    updated_lines = []

    for line in lines:
        if line.strip().startswith('@code'):
            in_code_block = True
            code_language = line.strip().split(' ')[-1]
            code_lines = []
        elif line.strip() == '@end':
            in_code_block = False
            html_code_block = '<pre><code class="{}">\n{}\n</code></pre>'.format(
                code_language,
                '\n'.join(code_lines).rstrip()
            )
            updated_lines.append(html_code_block)
        elif in_code_block:
            code_lines.append(line)
        else:
            updated_lines.append(line)

    return '\n'.join(updated_lines)


def h_method(content, level, icon):
    lines = content.splitlines()
    updated_lines = [
        f'<h{level}>{icon} {line.replace("*" * level + " ", "").strip()}</h{level}>' if line.startswith('*' * level + ' ') else line
        for line in lines
    ]
    return '\n'.join(updated_lines)

def convert_ordered_list_items(text):
    # Regular expression to match lines starting with a number followed by a period
    ordered_list_pattern = re.compile(r'^\s*(\d+\.)[^\S\n]*(.*)$')

    # Split the text into lines
    lines = text.splitlines()

    # Variables to keep track of the state
    in_ordered_list = False
    updated_lines = []

    for line in lines:
        match = ordered_list_pattern.match(line)
        if match:
            # If a line matches the pattern, handle it as part of an ordered list
            if not in_ordered_list:
                # If not already in an ordered list, start a new one
                updated_lines.append('<ol>')
                in_ordered_list = True

            # Replace the number with an <li> tag
            li_content = match.group(2).strip()
            updated_lines.append(f'  <li>{li_content}</li>')
        else:
            # If the line doesn't match, close the ordered list if needed
            if in_ordered_list:
                updated_lines.append('</ol>')
                in_ordered_list = False

            # Keep the original line
            updated_lines.append(line)

    # Close the ordered list if it's still open
    if in_ordered_list:
        updated_lines.append('</ol>')

    # Combine the lines back into a single string
    updated_text = '\n'.join(updated_lines)
    return updated_text

def ul_method(content):
    updated_content = []
    in_list = False

    for line in content.splitlines():
        if line.lstrip().startswith('- '):
            if not in_list:
                updated_content.append('<ul>\n')
            in_list = True
            updated_content.append(f'  <li>{line[2:].replace("- ", "").strip()}</li>\n')
        elif line.lstrip().startswith('-- '):
            if not in_list:
                updated_content.append('<ul>\n')
                in_list = True
            updated_content.append(f'  <li class="nested_list">{line[3:].replace("-- ", "").strip()}</li>\n')
        else:
            if in_list:
                updated_content.append('</ul>\n')
                in_list = False
            updated_content.append(line)

    return '\n'.join(updated_content)

def fix_section_encapsulation(content):
    # Define a regular expression pattern for headings and the text between them
    pattern = re.compile(r'<h([1-6])>(.*?)<\/h\1>(.*?)(?=<h[1-6]>|$)', re.DOTALL)

    # Find all matches in the content
    matches = pattern.findall(content)

    # Iterate through matches and update content
    for level, heading_text, following_text in matches:
        # Skip lines starting with specified indicators
        if re.search(r'^\s*({#|\{\*|{:|http|https|ftp|photo|link})', following_text):
            continue

        # Skip <p> tags for exceptions (@code, @end, <pre>)
        if re.search(r'@(?:code|end)|<pre>.*<\/pre>', following_text, re.DOTALL):
            continue

        # Add <p> tags if the following text is not already encapsulated and is not one of the exceptions
        if not following_text.strip() or not re.search(r'<(?:p|ol|ul|strong|u|em|s)>.*<\/(?:p|ol|ul|strong|u|em|s)>', following_text, re.DOTALL):
            indented_text = f'  {following_text.strip()}'  # Remove whitespace from both sides and add two spaces
            
            # Check for special case to ignore content inside curly braces starting with * or #
            if re.search(r'\{\*.*?\*\}', indented_text) or re.search(r'\{#.*?#\}', indented_text):
                updated_text = f'{indented_text}\n'
            else:
                # Adjust text size based on <h> tag with a minimum font size of 10px
                font_size = max(10, 14 - (int(level) * 2))
                updated_text = f'<p style="font-size: {font_size}px;">{indented_text}</p>\n'

            # Replace the original heading and text with encapsulated text
            content = content.replace(f'<h{level}>{heading_text}</h{level}>{following_text}',
                                      f'<h{level} style="font-size: {font_size}px;">{heading_text}</h{level}>\n{updated_text}', 1)

    return content

def add_underline_tags(text):
    def tokenize_lines(lines):
        # Tokenize the input text into lines
        for line in lines:
            yield line

    lines = text.splitlines()

    # Regular expression to match words surrounded by single underscores
    underscore_pattern = re.compile(r'(?<![_\w])_([^_]+)_(?![_\w])')

    # Lines to ignore for adding underline tags (e.g., code blocks)
    ignore_lines = set()

    # Iterate through the lines and tokenize
    tokens = tokenize_lines(lines)
    updated_lines = []

    for line in tokens:
        # Check if the line should be ignored
        ignore = any(line.lstrip().startswith(prefix) for prefix in ['<pre>', '```'])
        if ignore:
            ignore_lines.add(line)
        elif line in ignore_lines:
            ignore_lines.remove(line)
        else:
            # Add underline tags to matched words
            updated_line = underscore_pattern.sub(r'<u>\1</u>', line)
            updated_lines.append(updated_line)

    # Combine the lines back into a single string
    updated_text = '\n'.join(updated_lines)
    return updated_text

def add_strike_through_tags(text):
    # Regular expression to match single hyphens
    strike_through_pattern = re.compile(r'(?<!\S)-(?!\s)(.*?)(?<!\s)-(?!\S)')

    # Add strike-through tags to matched words
    updated_text = strike_through_pattern.sub(r'<s>\1</s>', text)

    return updated_text

def add_strong_tags(text):
    # Regular expression to match words surrounded by '*'
    strong_pattern = re.compile(r'\*(.*?)\*')

    # Replace the opening and closing '*' with '<strong>' and '</strong>'
    updated_text = strong_pattern.sub(r'<strong>\1</strong>', text)

    return updated_text

def add_em_tags(text):
    # Regular expression to match words surrounded by '/'
    em_pattern = re.compile(r'/(?<!@)([^/\n]+)/(?!@)')

    # Skip lines that appear to be URLs or start with 'link:'
    lines = text.splitlines()
    updated_lines = []

    for line in lines:
        if re.match(r'^\s*(http:\/\/|https:\/\/|ftp:\/\/|link\/\/|photo\/\/)', line):
            # Preserve URLs without modification
            updated_lines.append(line)
        elif line.lstrip().startswith('link'):
            # Preserve lines starting with 'link:'
            updated_lines.append(line)
        elif line.lstrip().startswith('photo'):
            # Preserve lines starting with 'link:'
            updated_lines.append(line)
        else:
            # Replace the opening and closing '/' with '<em>' and '</em>'
            updated_line = em_pattern.sub(r'<em>\1</em>', line)
            updated_lines.append(updated_line)

    return '\n'.join(updated_lines)

def match_tmp_to_html(tmp_file_path='html/tmp1.txt', html_file_path=f'html/{PF}.html'):
    # Read the content from tmp1.txt
    with open(tmp_file_path, 'r') as tmp_file:
        tmp_content = tmp_file.read()

    # Read the content from the HTML file
    with open(html_file_path, 'r') as html_file:
        html_content = html_file.read()

    # Extract namespaces and replace them in the HTML content
    namespace_pattern = re.compile(r'__YANKED_CONTENT__\d+__\s*({[^}]+}|http[^\s]+|ftp[^\s]+)')
    matches = namespace_pattern.findall(tmp_content)

    for match in matches:
        html_content = html_content.replace(f'__YANKED_CONTENT__{matches.index(match) + 1}__', match)

    # Write the modified HTML content back to the file
    with open(html_file_path, 'w') as html_file:
        html_file.write(html_content)

# Call match_tmp_to_html after creating the HTML file and tmp1.txt
# match_tmp_to_html()

def destroy_tmp_files():
    tmp_file_path = os.path.join("html", "tmp.txt")
    tmp1_file_path = os.path.join("html", "tmp1.txt")

    # Check if the files exist before attempting to delete
    if os.path.exists(tmp_file_path):
        os.remove(tmp_file_path)
        print(f'{tmp_file_path} deleted.')
    else:
        print(f'{tmp_file_path} not found.')

    if os.path.exists(tmp1_file_path):
        os.remove(tmp1_file_path)
        print(f'{tmp1_file_path} deleted.')
    else:
        print(f'{tmp1_file_path} not found.')

def generate_table_of_contents(content):
    # Define a regular expression pattern for <h1> and <h2> tags
    pattern = re.compile(r'<h([1-2])>(.*?)<\/h\1>', re.DOTALL)

    # Find all matches in the content
    matches = pattern.findall(content)

    # Generate the table of contents
    toc = "<h2>Table of Contents</h2>\n<ul>"
    for level, title in matches:
        # Remove unwanted part from the title
        cleaned_title = re.sub(r'[^a-zA-Z0-9\s]', '', title)
        # Create an anchor link for each section without leading dash
        anchor_link = f'<a href="#{cleaned_title.lower().replace(" ", "-").lstrip("-")}">{title}</a>'
        toc += f'\n<li>{anchor_link}</li>'

    toc += "\n</ul>"

    # Replace the first occurrence of <h1> or <h2> with the table of contents
    updated_content = pattern.sub('', content, count=1)
    updated_content = f"{toc}\n{updated_content}"

    return updated_content

def convert_http_to_img_with_prompt(text):
    # Regular expression to match lines starting with http
    http_pattern = re.compile(r'^\s*(https:\/\/)[^\s]+')

    # Split the text into lines
    lines = text.splitlines()

    # Iterate through the lines and replace http lines with <img> tags
    updated_lines = []
    for line in lines:
        match = http_pattern.match(line)
        if match:
            url = match.group().strip()

            # Prompt the user for the type of content (link, photo, or video)
            content_type = input(f'What type of content is the URL "{url}"? ((L)ink, (P)hoto, (V)ideo): ').upper()

            if content_type == 'L':
                # If it's a link, keep the original line
                link_tag = f'<a href="{url}">Click Here</a>'
                updated_lines.append(link_tag)
            elif content_type == 'P':
                photo_justify = input("What justification do you want? (L)eft, (R)ight, or (C)enter: ").upper()

                if photo_justify == "L" or photo_justify == "R":
                    # Prompt the user for the aspect ratio
                    aspect_ratio_choice = input("Choose the aspect ratio:\n1. 16:9 (Horizontal)\n2. 1:1 (Square)\nEnter the number corresponding to your choice: ")
                    if aspect_ratio_choice == "1":
                        ratio = "horizontal"
                    elif aspect_ratio_choice == "2":
                        ratio = "square"

                    img_tag = f'<img class="float-{photo_justify.lower()} {ratio}" src="{url}" />'
                    updated_lines.append(img_tag)
                elif photo_justify == "C":
                    # Center justification, use default resolution
                    img_tag = f'<img class="float-center" src="{url}" />'
                    updated_lines.append(img_tag)
                else:
                    print("Invalid justification choice.")
            elif content_type == 'V':
                # If it's a video, you can handle it accordingly
                # For now, keep the original line
                video_tag = f'<video width="640" src="{url}"></video>'
                updated_lines.append(video_tag)
            else:
                # If the user enters an invalid option, keep the original line
                updated_lines.append(line)
        else:
            updated_lines.append(line)

    # Combine the lines back into a single string
    updated_text = '\n'.join(updated_lines)
    return updated_text

# Apply the methods one by one
content = open_file()
check_html_file()
create_tmp_files()
content = h_method(content, 1, '\uF192')
content = h_method(content, 2, '\uF111')
content = h_method(content, 3, '\uF22d')
content = h_method(content, 4, '\uF666')
content = h_method(content, 5, '\uF292')
content = h_method(content, 6, '\uF0eb')
content = convert_ordered_list_items(content)
content = ul_method(content)
content = fix_section_encapsulation(content)
content = add_em_tags(content)
content = add_strike_through_tags(content)
content = add_underline_tags(content)
content = add_strong_tags(content)
content = convert_code_blocks(content)
content = generate_table_of_contents(content)
content = convert_http_to_img_with_prompt(content)

CSS_CODE = """
<style>
    h2 {margin-left: 10px; }
    h3 {margin-left: 20px; }
    h4 {margin-left: 30px; }
    h5 {margin-left: 40px; }
    h6 {margin-left: 50px; }
    ol {margin-left: 20px; }
    ul {margin-left: 20px; }
    p {margin-left: 20px; }
    .nested_list {margin-left: 40px;}
    .float-left {margin-right: 6px;}
    .float-right {margin-left: 6px; }
    .float-center {margin-left: auto; margin-right: auto; width: 1280px; height: 720px; }
    .horizontal {width: 640; height: 640;}
    .square {width: 512; height: 512;}

</style> 
"""

# Writing to the output file
with open(f"html/{PF}.html", "w") as output_file:
    output_file.write(content)
    output_file.write(CSS_CODE)

yank_from_html_and_save_to_tmp()
create_namespaces()
paste_namespaces()
match_tmp_to_html()
destroy_tmp_files()

