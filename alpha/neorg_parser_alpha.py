#!/usr/bin/python3
import os
import re

PROMPT = input('Would you like to parse a norg file into html? ').upper()

if PROMPT == 'Y':
    for fname in os.listdir('.'):
        if fname.endswith('.norg'):
            continue
elif PROMPT == 'N':
    print('See you later alligator.')
    exit()
else:
    print('Please try again with a valid input.')
    exit()

def get_base():
    files = os.listdir('.')
    filenames_without_extensions = []
    for f in files:
        filename, extension = os.path.splitext(f)
        filenames_without_extensions.append(filename)

    return str(filenames_without_extensions)

BASE = get_base()
print(BASE)

def declare_file_to_parse():
    """declare which file base name to parse"""
    return input("Which file do you want to parse into HTML (Please don't include file extension)? ")

PF = declare_file_to_parse()

def check_html_file():
    """Creating an html folder if it already exists."""
    if not os.path.exists('html'):
        os.makedirs('html')
check_html_file()

with open(f"{PF}.norg", 'r') as input_file:
    content = input_file.read()

def h_method(content, level, icon):
    lines = content.splitlines()
    updated_lines = [
        f'<h{level}>{icon} {line.replace("*" * level + " ", "").strip()}</h{level}>' if line.startswith('*' * level + ' ') else line
        for line in lines
    ]
    return '\n'.join(updated_lines)

def ul_method(content):
    updated_content = []
    in_list = False

    for line in content.splitlines():
        if line.lstrip().startswith('- '):
            if not in_list:
                updated_content.append('<ul>\n')
in_list = True
            updated_content.append(f'  <li>{line[2:].replace("- ", "").strip()}</li>\n')
        elif line.lstrip().startswith('-- '):
            if not in_list:
                updated_content.append('<ul>\n')
                in_list = True
            updated_content.append(f'  <li class="nested_list">{line[3:].replace("-- ", "").strip()}</li>\n')
        else:
            if in_list:
                updated_content.append('</ul>\n')
                in_list = False
            updated_content.append(line)

    if in_list:
        updated_content.append('</ul>\n')

    return '\n'.join(updated_content)


def convert_code_blocks(content):
    lines = content.split('\n')
    in_code_block = False
    code_language = ''
    code_lines = []
    updated_lines = []

    for line in lines:
        if line.strip().startswith('@code'):
            in_code_block = True
            code_language = line.strip().split(' ')[-1]
            code_lines = []
        elif line.strip() == '@end':
            in_code_block = False
            html_code_block = '<pre><code class="{}">\n{}\n</code></pre>'.format(
                code_language,
                '\n'.join(code_lines).rstrip()
            )
            updated_lines.append(html_code_block)
        elif in_code_block:
            code_lines.append(line)
        else:
            updated_lines.append(line)

    return '\n'.join(updated_lines)

def stylize_fonts(content):
    # Define a regular expression pattern for bold, underline, italicize, and strike-through
    # Preserve patterns for {::}, {*}, and {#} blocks
    pattern = re.compile(r'\*(.*?)\*|_(.*?)_|\/(.*?)\/|\-(.*?)(?=\W|$)|(\b\w+-\w+\b)|(\{:.+?:\})|(\{\*.+?\})|(\{#.+?#\})')

    def replace(match):
        # Extract all groups from the match
        groups = match.groups()

        # Include a hyphen between </s> and <s> only for strike-through with hyphenated words
        result = ''.join([
            f'<strong>{groups[0]}</strong>' if groups[0] else '',
            f'<u>{groups[1]}</u>' if groups[1] else '',
            f'<em>{groups[2]}</em>' if groups[2] else '',
            f'<s>{groups[3]}-{groups[4]}</s>' if groups[3] and groups[4] else '',
            f'<s>{groups[3]}</s>{groups[4]}' if groups[3] and not groups[4] else '',
            f'{groups[5]}' if groups[5] else '',  # Preserve {::} block as is
            f'{groups[6]}' if groups[6] else '',  # Preserve {*} block as is
            f'{groups[7]}' if groups[7] else ''   # Preserve {# ... #} block as is
        ])

        # Remove 'None' and the second dash
        return result.replace('None', '-')

    # Use re.sub with the replacement function
    updated_content = pattern.sub(replace, content)

    # Remove trailing <s></s> tags
    #updated_content = re.sub(r'<s></s>\s*', '', updated_content)
    return updated_content

def convert_image_blocks(content):
    lines = content.split('\n')
    updated_lines = []

    i = 0
    while i < len(lines):
        line = lines[i].strip()

        if line.startswith('@image'):
            # Prompt the user for the image URL
            img_url = input("Enter the image URL: ")
            # Process the image block and replace it with the img tag
            img_tag = f'<img src="{img_url}" />'
            updated_lines.append(img_tag)
        elif line.startswith('@endimage'):
            # Skip the @endimage line
            pass
        elif '{:' in line or '{*' in line or '{#' in line:
            # Preserve content inside {: ... :}, {* ...}, and {# ... #}
            while '}' not in line:
                updated_lines.append(line)
                i += 1
                line = lines[i].strip()
            updated_lines.append(line)
        else:
            updated_lines.append(line)

        i += 1

    return '\n'.join(updated_lines)


def generate_table_of_contents(content):
    # Define a regular expression pattern for <h1> and <h2> tags
    pattern = re.compile(r'<h([1-2])>(.*?)<\/h\1>', re.DOTALL)

    # Find all matches in the content
    matches = pattern.findall(content)

    # Generate the table of contents
    toc = "<h2>Table of Contents</h2>\n<ul>"
    for level, title in matches:
        # Remove unwanted part from the title
        cleaned_title = re.sub(r'[^a-zA-Z0-9\s]', '', title)
        # Create an anchor link for each section without leading dash
        anchor_link = f'<a href="#{cleaned_title.lower().replace(" ", "-").lstrip("-")}">{title}</a>'
        toc += f'\n<li>{anchor_link}</li>'

    toc += "\n</ul>"

    # Replace the first occurrence of <h1> or <h2> with the table of contents
    updated_content = pattern.sub('', content, count=1)
    updated_content = f"{toc}\n{updated_content}"

    return updated_content

def fix_section_encapsulation(content):
    # Define a regular expression pattern for headings and the text between them
    pattern = re.compile(r'<h([1-6])>(.*?)<\/h\1>(.*?)(?=<h[1-6]>|$)', re.DOTALL)

    # Find all matches in the content
    matches = pattern.findall(content)

    # Iterate through matches and update content
    for level, heading_text, following_text in matches:
        # Skip <p> tags for exceptions (@code, @end, <pre>)
        if re.search(r'@(?:code|end)|<pre>.*<\/pre>', following_text, re.DOTALL):
            continue

        # Add <p> tags if the following text is not already encapsulated and is not one of the exceptions
        if following_text and not re.search(r'<(?:p|ol|ul|strong|u|em|s)>.*<\/(?:p|ol|ul|strong|u|em|s)>', following_text, re.DOTALL):
            indented_text = f'  {following_text.strip()}'  # Remove whitespace from both sides and add two spaces
            
            # Check for special case to ignore content inside curly braces starting with * or #
            if re.search(r'\{\*.*?\*\}', indented_text) or re.search(r'\{#.*?#\}', indented_text):
                updated_text = f'{indented_text}\n'
            else:
                updated_text = f'<p>{indented_text}</p>\n'
                
                # Adjust text size based on <h> tag
                font_size = f'font-size: {14 - (int(level) * 2)}px;'
                updated_text = f'<p style="{font_size}">{indented_text}</p>\n'

            # Replace the original heading and text with encapsulated text
            content = content.replace(f'<h{level}>{heading_text}</h{level}>{following_text}',
                                      f'<h{level} style="{font_size}">{heading_text}</h{level}>\n{updated_text}')

    return content

# Apply the methods one by one
content = h_method(content, 1, '\uF192')
content = h_method(content, 2, '\uF111')
content = h_method(content, 3, '\uF22d')
content = h_method(content, 4, '\uF666')
content = h_method(content, 5, '\uF292')
content = h_method(content, 6, '\uF0eb')
content = ul_method(content)
content = stylize_fonts(content)
content = convert_code_blocks(content)
content = generate_table_of_contents(content)
content = fix_section_encapsulation(content)
content = convert_image_blocks(content)

CSS_CODE = """
<style>
    h2 {margin-left: 10px; }
    h3 {margin-left: 20px; }
    h4 {margin-left: 30px; }
    h5 {margin-left: 40px; }
    h6 {margin-left: 50px; }
    ol {margin-left: 20px; }
    ul {margin-left: 20px; }
    p {margin-left: 20px; }
    .nested_list {margin-left: 40px; }
</style> 
"""

# Writing to the output file
with open(f"html/{PF}.html", "w") as output_file:
    output_file.write(content)
    output_file.write(CSS_CODE)

