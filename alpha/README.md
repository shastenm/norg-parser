# Norg-Parser
## Overview
Welcome to Norg-Parser, a Python3 script designed to seamlessly integrate with Neorg in Neovim. This tool enhances your writing experience by allowing you to convert Norg files to HTML directly from the command line.

## Features
- **Alpha Stage:** The parser is currently in the Alpha stage, offering functionality that generally works well. However, be aware of potential edge cases and unresolved issues. Your feedback is essential for refining and improving the parser.

- **TailwindCSS Integration (Planned):** Future versions may include an option to use TailwindCSS instead of plain CSS, providing more control over indentation and styling.

- **Multi-Format Support (Planned):**  Upcoming updates will introduce parsers for Markdown, Markdown-GitLab, and Pandoc Markdown, making it a versatile tool for different writing formats.

- **GUI Support (Planned):** In the pipeline is the development of a graphical user interface (GUI) to streamline the file selection process and enhance user interaction.

## Installation
To get started, follow these *steps*:

1. Clone the repository:

```bash
git clone https://github.com/your-username/norg-parser.git
```

2. Navigate to the *project directory*:
``` bash
cd norg-parser
```
This command takes you to the directory containing the Norg-Parser script.

 3. Make the script *executable*:

```bash
chmod +x norg_parser.py
```
4 **Execute** the script using the *command line*:

```bash
./norg_parser.py
```
Follow the prompts to select the file you want to parse, and the script will generate an HTML directory with the parsed content.

## Known Issues
- **Order of Parsing:** Currently, the parser processes formatting (e.g., bold, underline, italics) into HTML before handling exceptions. This may lead to challenges, and feedback on specific cases is appreciated.

- **URL Modification:** There is an issue with URL modification before sandboxing or protection from font-styling functionality. My current solution to this problem is users are prompted to enter the URL separately and call it after font-styling is complete.

# Miscellaneous

## **Use Case: Django Blog Integration** 
Norg-Parser is designed with a specific use case in mind – seamlessly integrating your blog posts into Django. By copying and pasting the parsed HTML content directly into the text box, you can effortlessly publish your blogs in a format similar to Norg files.

## **Image Alignment Functionality (Planned)**
In future releases, we plan to include functionality for left, right, and center justification of images within your blog posts. This feature will provide additional flexibility for visual presentation.

# **Feedback and Contributions**
Your feedback is invaluable in improving Norg-Parser. If you encounter any issues or have suggestions for enhancements, please open an issue on this Gitlab repository.

Happy coding!
