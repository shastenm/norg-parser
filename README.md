<h1 style="font-size: 12px;">Norg-Parser Beta Release</h1>
<p style="font-size: 12px;">  </p>
<h2 style="font-size: 10px;">Use Case: Django Blog Integration</h2>
<p style="font-size: 10px;">  Norg-Parser's Beta version is crafted to seamlessly integrate with your Django blog. By simply copying and pasting the parsed HTML content directly into the text box, you can effortlessly publish your blogs in a format similar to Norg files. This release further refines the integration process, making it easier for you to share your thoughts in a clean and consistent format.</p>
<h1 style="font-size: 12px;">New Features</h1>
<p style="font-size: 12px;">  </p>
<h2 style="font-size: 10px;">Image Alignment Functionality</h2>
<p style="font-size: 10px;">  One of the notable additions in this Beta release is the implementation of image alignment functionality. Now, you have the flexibility to align your images to the left, right, or center, enhancing the visual appeal of your blog posts. This feature adds a layer of customization to your content presentation, allowing you to showcase images in the way that best complements your writing.</p>

To upgrade to the Beta version, follow these steps:

<ol>
  <li>Clone the repository:</li>
</ol>

``` bash
git clone https://gitlab.com/shastenm/norg-parser.git
```


<ol>
  <li>Navigate to the project directory and make the script executable:</li>
</ol>

``` bash
cd norg-parser; chmod +x neorg_parser_beta.py
```

<ol>
  <li>Run it just like any other Bash script</li>
</ol>

``` bash
./neorg_parser_beta
```

<p>Follow the prompts to select the file you want to parse, and the script will generate an HTML directory with the parsed content.</p>

<h2 style="font-size: 10px;">Known Tweaks</h2>
<p style="font-size: 10px;">  While the Beta version has addressed known issues regarding the order of parsing and URL modification, there might be a couple of tweaks pending. However, these tweaks are close enough that anyone interested in hacking on it a little can use the tool with minimal trouble. Your feedback on these tweaks is highly encouraged.</p>

 <strong>Feedback and Contributions</strong> 
<p>Your input is most welcome in refining Norg-Parser. If you encounter any issues, have suggestions, or discover opportunities for improvement, please open an issue on the Gitlab repository. Feel free to contribute to the project and make it even better!</p>

<h1>Update</h1>
<p>Markdown version coming next. I tested out the html format on this to see what I needed to change, and it seems like quite a lot, so I am just going to just write a markdown version in the coming days.</p>

<h4>Happy writing with <strong>Norg-Parser Beta!</strong></h4>
