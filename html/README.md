# Norg-Parser Beta Release

## Use Case: Django Blog Integration
Norg-Parser's Beta version is crafted to seamlessly integrate with your Django blog. By simply copying and pasting the parsed HTML content directly into the text box, you can effortlessly publish your blogs in a format similar to Norg files. This release further refines the integration process, making it easier for you to share your thoughts in a clean and consistent format.

## New Features
### Image Alignment Functionality
One of the notable additions in this Beta release is the implementation of image alignment functionality. Now, you have the flexibility to align your images to the left, right, or center, enhancing the visual appeal of your blog posts. This feature adds a layer of customization to your content presentation, allowing you to showcase images in the way that best complements your writing.

## Installation
To upgrade to the Beta version, follow these steps:
1. Clone the repository:
```bash
git clone https://gitlab.com/shastenm/norg-parser.git

```
2. Navigate to the project directory and make the script executable:
```bash
cd norg-parser; chmod +x neorg_parser_beta.py
```
3. Run it just like any other Bash script
```bash
./neorg_parser_beta
```
Follow the prompts to select the file you want to parse, and the script will generate an HTML directory with the parsed content.

# Known Tweaks
While the Beta version has addressed known issues regarding the order of parsing and URL modification, there might be a couple of tweaks pending. However, these tweaks are close enough that anyone interested in hacking on it a little can use the tool with minimal trouble. Your feedback on these tweaks is highly encouraged.

# Feedback and Contributions
Your input is most welcome in refining Norg-Parser. If you encounter any issues, have suggestions, or discover opportunities for improvement, please open an issue on the Gitlab repository. Feel free to contribute to the project and make it even better!

Happy writing with Norg-Parser Beta!






